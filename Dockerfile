FROM squidfunk/mkdocs-material

WORKDIR /docs

COPY . /docs

EXPOSE 8000

CMD ["serve", "--dev-addr=0.0.0.0:8000"]
